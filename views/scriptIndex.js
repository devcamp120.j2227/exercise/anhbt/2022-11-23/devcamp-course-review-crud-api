/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
var gAllCourses = gCoursesDB.courses;

/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(function() {
    onPageLoading();
})
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// hàm xử lý sự kiện page loading
function onPageLoading() {
    // điền row recommended
    var recommendedCourses = gAllCourses.slice(0,4);
    loadCoursesToRowRecommended(recommendedCourses);

    // điền row popular
    var popularCourses = getPopularCourses(gAllCourses).slice(0,4);
    loadCoursesToRowPopular(popularCourses);

    // điền row trending
    var trendingCourses = getTrendingCourses(gAllCourses).slice(0,4);
    loadCoursesToRowTrending(trendingCourses);
}
/*** REGION 4 - Common functions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/

// hàm trả về một jquery object card course mới. hàm này copy paste một card mẫu đã làm sẵn đang ẩn và điền dữ liệu vào clone, hiển thị và trả về card mới hoàn chỉnh
function newCourseCard(paramCourse) {
    var newCourse = $(".card-example").eq(0).clone();

    newCourse.show();
    newCourse.toggleClass("card-example");

    newCourse.find(".card-img-top").prop("src",paramCourse.coverImage);
    newCourse.find(".card-title").text(paramCourse.courseName);
    newCourse.find(".duration").text(paramCourse.duration);
    newCourse.find(".level").text(paramCourse.level);
    newCourse.find(".price").text(paramCourse.price);
    newCourse.find(".discount-price").text(paramCourse.discountPrice);
    newCourse.find(".teacher-name").text(paramCourse.teacherName);
    newCourse.find(".teacher-photo").prop("src", paramCourse.teacherPhoto);

    return newCourse;
}
// hàm trả về course với tham số course id đã cho
function getCourseById(paramCourseId) {
    return gAllCourses.find(({ id }) => id == paramCourseId);
}

// hàm điền courses trong tham số vào row recommended
function loadCoursesToRowRecommended( paramCourses ) {
    paramCourses.forEach(course => {
        var newCourse = newCourseCard(course)
        newCourse.appendTo("#row-recommended");
    });
}

// hàm điền courses trong tham số vào row popular
function loadCoursesToRowPopular( paramCourses ) {
    paramCourses.forEach(course => {
        var newCourse = newCourseCard(course)
        newCourse.appendTo("#row-popular");
    });
}

// hàm điền courses trong tham số vào row trending
function loadCoursesToRowTrending( paramCourses ) {
    paramCourses.forEach(course => {
        var newCourse = newCourseCard(course)
        newCourse.appendTo("#row-trending");
    });
}

// hàm kiểm tra nếu course popular. trả về true nếu course popular
function isCoursePopular(paramCourse)  {
    return paramCourse.isPopular;
}

// hàm trả về những courses popular trong tham số điền vào
function getPopularCourses (paramCourses) {
    var popularCourses = paramCourses.filter(isCoursePopular);
    return popularCourses;
}

// hàm kiểm tra nếu course trending. trả về true nếu course trending
function isCourseTrending(paramCourse)  {
    return paramCourse.isTrending;
}

// hàm trả về những courses trending trong tham số điền vào
function getTrendingCourses (paramCourses) {
    var trendingCourses = paramCourses.filter(isCourseTrending);
    return trendingCourses;
}